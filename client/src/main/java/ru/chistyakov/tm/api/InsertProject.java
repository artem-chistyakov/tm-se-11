
package ru.chistyakov.tm.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for insertProject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="insertProject"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://api.tm.chistyakov.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="descriptionProject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dateBeginProject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dateEndProject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "insertProject", propOrder = {
    "session",
    "name",
    "descriptionProject",
    "dateBeginProject",
    "dateEndProject"
})
public class InsertProject {

    protected Session session;
    protected String name;
    protected String descriptionProject;
    protected String dateBeginProject;
    protected String dateEndProject;

    /**
     * Gets the value of the session property.
     * 
     * @return
     *     possible object is
     *     {@link Session }
     *     
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     * 
     * @param value
     *     allowed object is
     *     {@link Session }
     *     
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the descriptionProject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionProject() {
        return descriptionProject;
    }

    /**
     * Sets the value of the descriptionProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionProject(String value) {
        this.descriptionProject = value;
    }

    /**
     * Gets the value of the dateBeginProject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateBeginProject() {
        return dateBeginProject;
    }

    /**
     * Sets the value of the dateBeginProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateBeginProject(String value) {
        this.dateBeginProject = value;
    }

    /**
     * Gets the value of the dateEndProject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateEndProject() {
        return dateEndProject;
    }

    /**
     * Sets the value of the dateEndProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateEndProject(String value) {
        this.dateEndProject = value;
    }

}
