
package ru.chistyakov.tm.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.chistyakov.tm.api package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AuthorizeUser_QNAME = new QName("http://api.tm.chistyakov.ru/", "authorizeUser");
    private final static QName _AuthorizeUserResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "authorizeUserResponse");
    private final static QName _DeserializateDomain_QNAME = new QName("http://api.tm.chistyakov.ru/", "deserializateDomain");
    private final static QName _DeserializateDomainResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "deserializateDomainResponse");
    private final static QName _FindAllUser_QNAME = new QName("http://api.tm.chistyakov.ru/", "findAllUser");
    private final static QName _FindAllUserResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "findAllUserResponse");
    private final static QName _FindUserById_QNAME = new QName("http://api.tm.chistyakov.ru/", "findUserById");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "findUserByIdResponse");
    private final static QName _GetURL_QNAME = new QName("http://api.tm.chistyakov.ru/", "getURL");
    private final static QName _GetURLResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "getURLResponse");
    private final static QName _LoadDomainJacksonJson_QNAME = new QName("http://api.tm.chistyakov.ru/", "loadDomainJacksonJson");
    private final static QName _LoadDomainJacksonJsonResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "loadDomainJacksonJsonResponse");
    private final static QName _LoadDomainJacksonXml_QNAME = new QName("http://api.tm.chistyakov.ru/", "loadDomainJacksonXml");
    private final static QName _LoadDomainJacksonXmlResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "loadDomainJacksonXmlResponse");
    private final static QName _PersistUser_QNAME = new QName("http://api.tm.chistyakov.ru/", "persistUser");
    private final static QName _PersistUserResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "persistUserResponse");
    private final static QName _RegistryAdmin_QNAME = new QName("http://api.tm.chistyakov.ru/", "registryAdmin");
    private final static QName _RegistryAdminResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "registryAdminResponse");
    private final static QName _RegistryUser_QNAME = new QName("http://api.tm.chistyakov.ru/", "registryUser");
    private final static QName _RegistryUserResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "registryUserResponse");
    private final static QName _SaveDomainJacksonJson_QNAME = new QName("http://api.tm.chistyakov.ru/", "saveDomainJacksonJson");
    private final static QName _SaveDomainJacksonJsonResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "saveDomainJacksonJsonResponse");
    private final static QName _SaveDomainJacksonXml_QNAME = new QName("http://api.tm.chistyakov.ru/", "saveDomainJacksonXml");
    private final static QName _SaveDomainJacksonXmlResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "saveDomainJacksonXmlResponse");
    private final static QName _SerializateDomain_QNAME = new QName("http://api.tm.chistyakov.ru/", "serializateDomain");
    private final static QName _SerializateDomainResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "serializateDomainResponse");
    private final static QName _UpdateUser_QNAME = new QName("http://api.tm.chistyakov.ru/", "updateUser");
    private final static QName _UpdateUserPassword_QNAME = new QName("http://api.tm.chistyakov.ru/", "updateUserPassword");
    private final static QName _UpdateUserPasswordResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "updateUserPasswordResponse");
    private final static QName _UpdateUserResponse_QNAME = new QName("http://api.tm.chistyakov.ru/", "updateUserResponse");
    private final static QName _User_QNAME = new QName("http://api.tm.chistyakov.ru/", "user");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.chistyakov.tm.api
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AuthorizeUser }
     * 
     */
    public AuthorizeUser createAuthorizeUser() {
        return new AuthorizeUser();
    }

    /**
     * Create an instance of {@link AuthorizeUserResponse }
     * 
     */
    public AuthorizeUserResponse createAuthorizeUserResponse() {
        return new AuthorizeUserResponse();
    }

    /**
     * Create an instance of {@link DeserializateDomain }
     * 
     */
    public DeserializateDomain createDeserializateDomain() {
        return new DeserializateDomain();
    }

    /**
     * Create an instance of {@link DeserializateDomainResponse }
     * 
     */
    public DeserializateDomainResponse createDeserializateDomainResponse() {
        return new DeserializateDomainResponse();
    }

    /**
     * Create an instance of {@link FindAllUser }
     * 
     */
    public FindAllUser createFindAllUser() {
        return new FindAllUser();
    }

    /**
     * Create an instance of {@link FindAllUserResponse }
     * 
     */
    public FindAllUserResponse createFindAllUserResponse() {
        return new FindAllUserResponse();
    }

    /**
     * Create an instance of {@link FindUserById }
     * 
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     * 
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link GetURL }
     * 
     */
    public GetURL createGetURL() {
        return new GetURL();
    }

    /**
     * Create an instance of {@link GetURLResponse }
     * 
     */
    public GetURLResponse createGetURLResponse() {
        return new GetURLResponse();
    }

    /**
     * Create an instance of {@link LoadDomainJacksonJson }
     * 
     */
    public LoadDomainJacksonJson createLoadDomainJacksonJson() {
        return new LoadDomainJacksonJson();
    }

    /**
     * Create an instance of {@link LoadDomainJacksonJsonResponse }
     * 
     */
    public LoadDomainJacksonJsonResponse createLoadDomainJacksonJsonResponse() {
        return new LoadDomainJacksonJsonResponse();
    }

    /**
     * Create an instance of {@link LoadDomainJacksonXml }
     * 
     */
    public LoadDomainJacksonXml createLoadDomainJacksonXml() {
        return new LoadDomainJacksonXml();
    }

    /**
     * Create an instance of {@link LoadDomainJacksonXmlResponse }
     * 
     */
    public LoadDomainJacksonXmlResponse createLoadDomainJacksonXmlResponse() {
        return new LoadDomainJacksonXmlResponse();
    }

    /**
     * Create an instance of {@link PersistUser }
     * 
     */
    public PersistUser createPersistUser() {
        return new PersistUser();
    }

    /**
     * Create an instance of {@link PersistUserResponse }
     * 
     */
    public PersistUserResponse createPersistUserResponse() {
        return new PersistUserResponse();
    }

    /**
     * Create an instance of {@link RegistryAdmin }
     * 
     */
    public RegistryAdmin createRegistryAdmin() {
        return new RegistryAdmin();
    }

    /**
     * Create an instance of {@link RegistryAdminResponse }
     * 
     */
    public RegistryAdminResponse createRegistryAdminResponse() {
        return new RegistryAdminResponse();
    }

    /**
     * Create an instance of {@link RegistryUser }
     * 
     */
    public RegistryUser createRegistryUser() {
        return new RegistryUser();
    }

    /**
     * Create an instance of {@link RegistryUserResponse }
     * 
     */
    public RegistryUserResponse createRegistryUserResponse() {
        return new RegistryUserResponse();
    }

    /**
     * Create an instance of {@link SaveDomainJacksonJson }
     * 
     */
    public SaveDomainJacksonJson createSaveDomainJacksonJson() {
        return new SaveDomainJacksonJson();
    }

    /**
     * Create an instance of {@link SaveDomainJacksonJsonResponse }
     * 
     */
    public SaveDomainJacksonJsonResponse createSaveDomainJacksonJsonResponse() {
        return new SaveDomainJacksonJsonResponse();
    }

    /**
     * Create an instance of {@link SaveDomainJacksonXml }
     * 
     */
    public SaveDomainJacksonXml createSaveDomainJacksonXml() {
        return new SaveDomainJacksonXml();
    }

    /**
     * Create an instance of {@link SaveDomainJacksonXmlResponse }
     * 
     */
    public SaveDomainJacksonXmlResponse createSaveDomainJacksonXmlResponse() {
        return new SaveDomainJacksonXmlResponse();
    }

    /**
     * Create an instance of {@link SerializateDomain }
     * 
     */
    public SerializateDomain createSerializateDomain() {
        return new SerializateDomain();
    }

    /**
     * Create an instance of {@link SerializateDomainResponse }
     * 
     */
    public SerializateDomainResponse createSerializateDomainResponse() {
        return new SerializateDomainResponse();
    }

    /**
     * Create an instance of {@link UpdateUser }
     * 
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link UpdateUserPassword }
     * 
     */
    public UpdateUserPassword createUpdateUserPassword() {
        return new UpdateUserPassword();
    }

    /**
     * Create an instance of {@link UpdateUserPasswordResponse }
     * 
     */
    public UpdateUserPasswordResponse createUpdateUserPasswordResponse() {
        return new UpdateUserPasswordResponse();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     * 
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "authorizeUser")
    public JAXBElement<AuthorizeUser> createAuthorizeUser(AuthorizeUser value) {
        return new JAXBElement<AuthorizeUser>(_AuthorizeUser_QNAME, AuthorizeUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "authorizeUserResponse")
    public JAXBElement<AuthorizeUserResponse> createAuthorizeUserResponse(AuthorizeUserResponse value) {
        return new JAXBElement<AuthorizeUserResponse>(_AuthorizeUserResponse_QNAME, AuthorizeUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeserializateDomain }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "deserializateDomain")
    public JAXBElement<DeserializateDomain> createDeserializateDomain(DeserializateDomain value) {
        return new JAXBElement<DeserializateDomain>(_DeserializateDomain_QNAME, DeserializateDomain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeserializateDomainResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "deserializateDomainResponse")
    public JAXBElement<DeserializateDomainResponse> createDeserializateDomainResponse(DeserializateDomainResponse value) {
        return new JAXBElement<DeserializateDomainResponse>(_DeserializateDomainResponse_QNAME, DeserializateDomainResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "findAllUser")
    public JAXBElement<FindAllUser> createFindAllUser(FindAllUser value) {
        return new JAXBElement<FindAllUser>(_FindAllUser_QNAME, FindAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "findAllUserResponse")
    public JAXBElement<FindAllUserResponse> createFindAllUserResponse(FindAllUserResponse value) {
        return new JAXBElement<FindAllUserResponse>(_FindAllUserResponse_QNAME, FindAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetURL }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "getURL")
    public JAXBElement<GetURL> createGetURL(GetURL value) {
        return new JAXBElement<GetURL>(_GetURL_QNAME, GetURL.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetURLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "getURLResponse")
    public JAXBElement<GetURLResponse> createGetURLResponse(GetURLResponse value) {
        return new JAXBElement<GetURLResponse>(_GetURLResponse_QNAME, GetURLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainJacksonJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "loadDomainJacksonJson")
    public JAXBElement<LoadDomainJacksonJson> createLoadDomainJacksonJson(LoadDomainJacksonJson value) {
        return new JAXBElement<LoadDomainJacksonJson>(_LoadDomainJacksonJson_QNAME, LoadDomainJacksonJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainJacksonJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "loadDomainJacksonJsonResponse")
    public JAXBElement<LoadDomainJacksonJsonResponse> createLoadDomainJacksonJsonResponse(LoadDomainJacksonJsonResponse value) {
        return new JAXBElement<LoadDomainJacksonJsonResponse>(_LoadDomainJacksonJsonResponse_QNAME, LoadDomainJacksonJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainJacksonXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "loadDomainJacksonXml")
    public JAXBElement<LoadDomainJacksonXml> createLoadDomainJacksonXml(LoadDomainJacksonXml value) {
        return new JAXBElement<LoadDomainJacksonXml>(_LoadDomainJacksonXml_QNAME, LoadDomainJacksonXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainJacksonXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "loadDomainJacksonXmlResponse")
    public JAXBElement<LoadDomainJacksonXmlResponse> createLoadDomainJacksonXmlResponse(LoadDomainJacksonXmlResponse value) {
        return new JAXBElement<LoadDomainJacksonXmlResponse>(_LoadDomainJacksonXmlResponse_QNAME, LoadDomainJacksonXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "persistUser")
    public JAXBElement<PersistUser> createPersistUser(PersistUser value) {
        return new JAXBElement<PersistUser>(_PersistUser_QNAME, PersistUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "persistUserResponse")
    public JAXBElement<PersistUserResponse> createPersistUserResponse(PersistUserResponse value) {
        return new JAXBElement<PersistUserResponse>(_PersistUserResponse_QNAME, PersistUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryAdmin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "registryAdmin")
    public JAXBElement<RegistryAdmin> createRegistryAdmin(RegistryAdmin value) {
        return new JAXBElement<RegistryAdmin>(_RegistryAdmin_QNAME, RegistryAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryAdminResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "registryAdminResponse")
    public JAXBElement<RegistryAdminResponse> createRegistryAdminResponse(RegistryAdminResponse value) {
        return new JAXBElement<RegistryAdminResponse>(_RegistryAdminResponse_QNAME, RegistryAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "registryUser")
    public JAXBElement<RegistryUser> createRegistryUser(RegistryUser value) {
        return new JAXBElement<RegistryUser>(_RegistryUser_QNAME, RegistryUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "registryUserResponse")
    public JAXBElement<RegistryUserResponse> createRegistryUserResponse(RegistryUserResponse value) {
        return new JAXBElement<RegistryUserResponse>(_RegistryUserResponse_QNAME, RegistryUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainJacksonJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "saveDomainJacksonJson")
    public JAXBElement<SaveDomainJacksonJson> createSaveDomainJacksonJson(SaveDomainJacksonJson value) {
        return new JAXBElement<SaveDomainJacksonJson>(_SaveDomainJacksonJson_QNAME, SaveDomainJacksonJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainJacksonJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "saveDomainJacksonJsonResponse")
    public JAXBElement<SaveDomainJacksonJsonResponse> createSaveDomainJacksonJsonResponse(SaveDomainJacksonJsonResponse value) {
        return new JAXBElement<SaveDomainJacksonJsonResponse>(_SaveDomainJacksonJsonResponse_QNAME, SaveDomainJacksonJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainJacksonXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "saveDomainJacksonXml")
    public JAXBElement<SaveDomainJacksonXml> createSaveDomainJacksonXml(SaveDomainJacksonXml value) {
        return new JAXBElement<SaveDomainJacksonXml>(_SaveDomainJacksonXml_QNAME, SaveDomainJacksonXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainJacksonXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "saveDomainJacksonXmlResponse")
    public JAXBElement<SaveDomainJacksonXmlResponse> createSaveDomainJacksonXmlResponse(SaveDomainJacksonXmlResponse value) {
        return new JAXBElement<SaveDomainJacksonXmlResponse>(_SaveDomainJacksonXmlResponse_QNAME, SaveDomainJacksonXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SerializateDomain }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "serializateDomain")
    public JAXBElement<SerializateDomain> createSerializateDomain(SerializateDomain value) {
        return new JAXBElement<SerializateDomain>(_SerializateDomain_QNAME, SerializateDomain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SerializateDomainResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "serializateDomainResponse")
    public JAXBElement<SerializateDomainResponse> createSerializateDomainResponse(SerializateDomainResponse value) {
        return new JAXBElement<SerializateDomainResponse>(_SerializateDomainResponse_QNAME, SerializateDomainResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "updateUser")
    public JAXBElement<UpdateUser> createUpdateUser(UpdateUser value) {
        return new JAXBElement<UpdateUser>(_UpdateUser_QNAME, UpdateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "updateUserPassword")
    public JAXBElement<UpdateUserPassword> createUpdateUserPassword(UpdateUserPassword value) {
        return new JAXBElement<UpdateUserPassword>(_UpdateUserPassword_QNAME, UpdateUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "updateUserPasswordResponse")
    public JAXBElement<UpdateUserPasswordResponse> createUpdateUserPasswordResponse(UpdateUserPasswordResponse value) {
        return new JAXBElement<UpdateUserPasswordResponse>(_UpdateUserPasswordResponse_QNAME, UpdateUserPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "updateUserResponse")
    public JAXBElement<UpdateUserResponse> createUpdateUserResponse(UpdateUserResponse value) {
        return new JAXBElement<UpdateUserResponse>(_UpdateUserResponse_QNAME, UpdateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.tm.chistyakov.ru/", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

}
