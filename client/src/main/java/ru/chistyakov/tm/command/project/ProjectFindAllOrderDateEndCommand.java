package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.Project;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.List;

public final class ProjectFindAllOrderDateEndCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pfadec";
    }

    @Override
    public @NotNull String getDescription() {
        return "выводит все проекты в порядке дня окончания";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        List<Project> projectCollection = serviceLocator.getProjectEndpoint().findAllProjectInOrderDateEnd(serviceLocator.getSession());
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (Project project : projectCollection)
            System.out.println("Project{" +
                    "userId='" + project.getUserId() + '\'' +
                    "id='" + project.getId() + '\'' +
                    "name='" + project.getName() + '\'' +
                    ", readinessStatus=" + project.getReadinessStatus() +
                    ", description='" + project.getDescription() + '\'' +
                    ", dateBeginProject=" + project.getDateBeginProject() +
                    ", dateEndProject=" + project.getDateEndProject() +
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
