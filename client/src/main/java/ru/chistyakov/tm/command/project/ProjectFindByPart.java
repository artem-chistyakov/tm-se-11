package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.Project;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Collection;

public class ProjectFindByPart extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pfbp";
    }

    @Override
    public @NotNull String getDescription() {
        return "Поиск проектов по части названия или описания";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите часть названия или описания");
        String part = serviceLocator.getScanner().nextLine();
        Collection<Project> projectCollection = serviceLocator.getProjectEndpoint().findByPartProjectNameOrDescription(serviceLocator.getSession(), part);
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (Project project : projectCollection)
            System.out.println("Project{" +
                    "userId='" + project.getUserId() + '\'' +
                    "id='" + project.getId() + '\'' +
                    "name='" + project.getName() + '\'' +
                    ", readinessStatus=" + project.getReadinessStatus() +
                    ", description='" + project.getDescription() + '\'' +
                    ", dateBeginProject=" + project.getDateBeginProject() +
                    ", dateEndProject=" + project.getDateEndProject() +
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR, RoleType.USUAL_USER};
    }
}
