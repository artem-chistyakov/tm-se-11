package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class ProjectInsertCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pic";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создание нового проекта";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите название проекта");
        final String nameProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите описание проекта");
        final String descriptionProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату начала проекта dd.MM.yyyy");
        final String dateBeginProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату окончания проекта dd.MM.yyyy");
        final String dateEndProject = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getProjectEndpoint().insertProject(serviceLocator.getSession(), nameProject, descriptionProject,
                dateBeginProject, dateEndProject) != null) System.out.println("Новый проект успешно создан");
        else throw new IllegalArgumentException("Ошибка создания проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
