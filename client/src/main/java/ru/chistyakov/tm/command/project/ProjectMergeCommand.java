package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.Project;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class ProjectMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pmc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Если проекта не существует создает новый, иначе его обновляет";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        final Project project = new Project();
        project.setId(projectId);
        if (serviceLocator.getProjectEndpoint().mergeProject(serviceLocator.getSession(), project) != null)
            System.out.println("Команда успешно выполнена");
        else throw new IllegalArgumentException("Ошибка выполнения команды");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
