package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class ProjectRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "prac";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаляет все проекты текущего пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        serviceLocator.getProjectEndpoint().removeAllProject(serviceLocator.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
