package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "prc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаляет проект по id";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getProjectEndpoint().removeProject(serviceLocator.getSession(), projectId) != null)
            System.out.println("Проект с таким id удален");
        else throw new IllegalArgumentException("Ошибка удаления проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
