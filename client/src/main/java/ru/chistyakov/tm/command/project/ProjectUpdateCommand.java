package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "puc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновляет проект";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите название проекта");
        final String loginProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите описание проекта");
        final String descriptionProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату начала проекта DD.MM.yyyy");
        final String dateBeginProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату окончания проекта DD.MM.yyyy");
        final String dateEndProject = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getProjectEndpoint().updateProject(serviceLocator.getSession(), projectId, loginProject, descriptionProject, dateBeginProject, dateEndProject) != null)
            System.out.println("Проект обновлен");
        else throw new IllegalArgumentException("Ошибка обновления проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
