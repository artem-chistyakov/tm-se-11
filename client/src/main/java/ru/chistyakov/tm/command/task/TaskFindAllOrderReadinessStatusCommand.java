package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.Task;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class TaskFindAllOrderReadinessStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "tfarsc";
    }

    @Override
    public @NotNull String getDescription() {
        return "выводит все задачи в порядке статуса готовности";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        final Collection<Task> collection = serviceLocator.getTaskEndpoint().findAllTaskInOrderReadinessStatus(serviceLocator.getSession());
        if (collection.isEmpty()) throw new IllegalArgumentException("Задачи не найдены");
        for (final Task task : collection)
            System.out.println("Task{" +
                    " userId='" + task.getUserId() + '\'' +
                    "projectId='" + task.getProjectId() + '\'' +
                    ", id='" + task.getId() + '\'' +
                    ", name='" + task.getName() + '\'' +
                    ", readinessStatus=" + task.getReadinessStatus() +
                    ", description='" + task.getDescription() + '\'' +
                    ", dateBeginTask=" + task.getDateBeginTask() +
                    ", dateEndTask=" + task.getDateEndTask() +
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
