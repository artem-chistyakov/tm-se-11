package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class TaskInsertCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tpc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создает новую задачу";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите название задачи");
        final String taskName = serviceLocator.getScanner().nextLine();
        System.out.println("Введите описание задачи");
        final String descriptionTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату начала задачи DD.MM.yyyy");
        final String dateBeginTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату окончания задачи DD.MM.yyyy");
        final String dateEndTask = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getTaskEndpoint().insertTask(serviceLocator.getSession(),
                projectId, taskName, descriptionTask, dateBeginTask, dateEndTask) != null)
            System.out.println("Новая задача создана");
        else throw new IllegalArgumentException("Ошибка создания задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
