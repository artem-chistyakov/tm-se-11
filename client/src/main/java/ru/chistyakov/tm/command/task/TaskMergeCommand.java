package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.Task;
import ru.chistyakov.tm.command.AbstractCommand;


public final class TaskMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tmc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Если задачи не существует создает новую, иначе её обновляет";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id задачи");
        final String idTask = serviceLocator.getScanner().nextLine();
        final Task task = new Task();
        task.setId(idTask);
        if (serviceLocator.getTaskEndpoint().mergeTask(serviceLocator.getSession(), task) != null)
            System.out.println("Команда выполнена");
        else throw new IllegalArgumentException("Ошибка выполнения команды");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
