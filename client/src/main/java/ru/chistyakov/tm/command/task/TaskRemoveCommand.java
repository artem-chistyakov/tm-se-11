package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "trc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление одной команды авторизованного пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id задачи");
        final String taskId = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getTaskEndpoint().removeTask(serviceLocator.getSession(), taskId) != null)
            System.out.println("Задача удалена");
        else throw new IllegalArgumentException("Ошибка удаления задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
