package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tuc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновляет команду";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id задачи");
        final String taskId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новое id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новое название задачи");
        final String taskName = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новое описание задачи");
        final String descriptionTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новую дату начала задачи DD.MM.yyyy");
        final String dateBeginTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новую дату окончания задачи DD.MM.yyyy");
        final String dateEndTask = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getTaskEndpoint().updateTask(serviceLocator.getSession(),
                taskId, projectId, taskName, descriptionTask, dateBeginTask, dateEndTask) != null)
            System.out.println("Задача обновлена успешно");
        else throw new IllegalArgumentException("Ошибка обновления задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
