package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.io.IOException;

public class UserLoadDomenJacksonJson extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "uldjj";
    }

    @Override
    public @NotNull String getDescription() {
        return "загрузка предметной области с использование jackson в формате json";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        serviceLocator.getUserEndpoint().loadDomainJacksonJson(serviceLocator.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
