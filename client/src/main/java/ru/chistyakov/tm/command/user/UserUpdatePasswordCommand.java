package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "uupc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение пароля ";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите старый пароль");
        if (serviceLocator.getUserEndpoint().updateUserPassword(serviceLocator.getSession(), serviceLocator.getScanner().nextLine()) != null)
            System.out.println("Пароль изменен");
        else System.out.println("Ошибка изменения пароля");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
