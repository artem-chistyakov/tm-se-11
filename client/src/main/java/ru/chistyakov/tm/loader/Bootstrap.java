package ru.chistyakov.tm.loader;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.endpoint.ProjectEndpointService;
import ru.chistyakov.tm.endpoint.SessionEndpointService;
import ru.chistyakov.tm.endpoint.TaskEndpointService;
import ru.chistyakov.tm.endpoint.UserEndpointService;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.*;

public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final static Set<Class<? extends AbstractCommand>> CLASSES = new Reflections("ru.chistyakov.tm.command").getSubTypesOf(AbstractCommand.class);
    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();
    @NotNull
    private final Scanner scanner = new Scanner(System.in);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    @Nullable
    private Session session = null;

    @NotNull
    @Override
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    @Override
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public ISessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Nullable
    @Override
    public Session getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

    @NotNull
    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }

    @NotNull
    @Override
    public Scanner getScanner() {
        return scanner;
    }

    private void registry(@NotNull final AbstractCommand abstractCommand) {
        commandMap.put(abstractCommand.getName(), abstractCommand);
    }

    private void registry(@NotNull final Class clazz) {
        try {
            final AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setServiceLocator(this);
            registry(command);
        } catch (final Exception exception) {
            exception.printStackTrace();
        }
    }

    private void registry(@NotNull final Set<Class<? extends AbstractCommand>> classes) {
        for (final Class clazz : classes) registry(clazz);
    }

    private void execute(@Nullable final AbstractCommand abstractCommand, @Nullable final Session session)
            throws NullPointerException, IllegalArgumentException, UnsupportedOperationException, IOException, JAXBException, ClassNotFoundException {
        if (abstractCommand == null) throw new UnsupportedOperationException("Вам не доступна данная команда.");
        final Collection<RoleType> roleTypeCollection = Arrays.asList(abstractCommand.getSupportedRoles());
        if (session == null) {
            if (!roleTypeCollection.contains(null))
                throw new UnsupportedOperationException("Вам не доступна данная команда.");
            abstractCommand.execute();
        } else {
            if (roleTypeCollection.contains(session.getRoleType())) {
                abstractCommand.execute();
            } else throw new UnsupportedOperationException("Вам не доступна данная команда.");
        }
    }


    private void init() {
        registry(Bootstrap.CLASSES);
    }

    public void start() {
        init();
        userEndpoint.registryAdmin("admin", "admin");
        userEndpoint.registryUser("user", "user");
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (true) {
            System.out.println("\n" + "Введите команду");
            final AbstractCommand abstractCommand = commandMap.get(scanner.nextLine());
            try {
                execute(abstractCommand, session);
            } catch (NullPointerException | IllegalArgumentException | UnsupportedOperationException | IOException | JAXBException | ClassNotFoundException exception) {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }
    }
}

