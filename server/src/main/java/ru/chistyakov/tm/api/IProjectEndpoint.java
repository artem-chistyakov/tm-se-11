package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectEndpoint {

    @NotNull
    String getURL();

    @WebMethod
    @Nullable Project persistProject(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                     @Nullable @WebParam(name = "project", partName = "project") Project project);

    @WebMethod
    @Nullable Project mergeProject(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                   @Nullable @WebParam(name = "project", partName = "project") Project project);

    @WebMethod
    @Nullable Project insertProject(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                    @Nullable @WebParam(name = "name", partName = "name") String name,
                                    @Nullable @WebParam(name = "descriptionProject", partName = "descriptionProject") String descriptionProject,
                                    @Nullable @WebParam(name = "dateBeginProject", partName = "dateBeginProject") String dateBeginProject,
                                    @Nullable @WebParam(name = "dateEndProject", partName = "dateEndProject") String dateEndProject);

    @WebMethod
    @Nullable Project findOneProject(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                     @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId);

    @WebMethod
    @Nullable Project removeProject(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                    @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId);

    @WebMethod
    @Nullable Project updateProject(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                    @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
                                    @Nullable @WebParam(name = "projectName", partName = "projectName") String projectName,
                                    @Nullable @WebParam(name = "descriptionProject", partName = "descriptionProject") String descriptionProject,
                                    @Nullable @WebParam(name = "dateBegin", partName = "dateBegin") String dateBegin,
                                    @Nullable @WebParam(name = "dateEnd", partName = "dateEnd") String dateEnd);

    @WebMethod
    @Nullable Collection<Project> findAllProjectByUserId(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Project> findAllProject(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Project> findAllProjectInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Project> findAllProjectInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Project> findAllProjectInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Project> findByPartProjectNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                                                     @Nullable @WebParam(name = "part", partName = "part") String part);

    @WebMethod
    @Nullable void removeAllProject(@Nullable @WebParam(name = "session", partName = "session") Session session);
}
