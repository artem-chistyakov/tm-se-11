package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {


    Project insert(@NotNull String userId, @NotNull String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    @Nullable
    Project persist(@NotNull Project project);

    @Nullable
    Project findOne(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Collection<Project> findAll(@NotNull String userId);

    @NotNull
    Collection<Project> findAllInOrder(@NotNull String userId, Comparator<Project> comparator);

    Project update(@NotNull String projectId, @NotNull String projectName, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    Project remove(@NotNull String userId, @NotNull String projectId);

    void removeAll(@NotNull String userId);

    Project merge(@NotNull Project project);

    @NotNull
    Collection<Project> findByPartNameOrDescription(String userId, String part);

    @NotNull
    Collection<Project> findAll();

    @NotNull List<Project> getList();
}
