package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectService extends IService<Project> {

    @WebMethod
    Project insert(@Nullable String userId, @Nullable String name, @Nullable String descriptionProject,
                   @Nullable String dateBegin, @Nullable String dateEnd);

    @WebMethod
    Project merge(@Nullable Project project);

    @WebMethod
    void removeAll(@Nullable String userId);

    @WebMethod
    @Nullable
    Project persist(@Nullable Project project);

    @WebMethod
    Project remove(@Nullable String userId, @Nullable String projectId);

    @WebMethod
    @Nullable
    Project findOne(@Nullable String userId, @Nullable String projectId);

    @WebMethod
    Project update(@Nullable String projectId, @Nullable String projectName, @Nullable String descriptionProject, @Nullable String dateBegin, @Nullable String dateEnd);

    @WebMethod
    @Nullable
    Collection<Project> findAll(String userId);

    @WebMethod
    @Nullable
    Collection<Project> findAllInOrderDateBegin(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<Project> findAllInOrderDateEnd(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<Project> findAllInOrderReadinessStatus(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<Project> findByPartNameOrDescription(@Nullable String userId, String part);

}