package ru.chistyakov.tm.api;

import ru.chistyakov.tm.entity.AbstractEntity;

public interface IRepository<T extends AbstractEntity> {

    T persist(T t);

    T merge(T t);
}
