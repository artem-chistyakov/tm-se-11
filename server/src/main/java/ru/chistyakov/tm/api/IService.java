package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.entity.Project;

import java.util.Collection;

public interface IService<T> {

    T merge(T t);

    T persist(T t);

    @NotNull
    Collection<T> findAll();
}
