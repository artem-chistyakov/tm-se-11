package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {
    @NotNull
    String getURL();

    @Nullable
    @WebMethod
    Session openSession(@Nullable @WebParam(name = "userId", partName = "userId") String userId,
                        @Nullable @WebParam(name = "roleType", partName = "roleType") RoleType roleType);

    @Nullable
    @WebMethod
    Session closeSession(@Nullable @WebParam(name = "session", partName = "session") Session session);
}
