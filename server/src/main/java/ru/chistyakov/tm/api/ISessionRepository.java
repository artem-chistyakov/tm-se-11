package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Map;

public interface ISessionRepository {

    Map<String, Session> getSessionMap();

    Session create(@Nullable String userId, RoleType roleType);

    boolean contains(@Nullable String sessionId);

    Session delete(@Nullable String sessionId);
}
