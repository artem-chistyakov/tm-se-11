package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;

public interface ISessionService {

    ISessionRepository getSessionRepository();

    void validate(@Nullable Session session) throws IllegalAccessException;

    void validate(@Nullable Session session,@Nullable RoleType roleType) throws IllegalAccessException;

    boolean isValid(@Nullable Session session);

    boolean isValid(@Nullable Session session, @Nullable RoleType roleType);

    @Nullable
    Session open(@Nullable String userId, RoleType roleType);

    @Nullable
    Session close(@Nullable Session session);
}
