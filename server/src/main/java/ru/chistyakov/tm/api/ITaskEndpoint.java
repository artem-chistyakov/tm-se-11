package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {
    @NotNull
    String getURL();
    @WebMethod
    Task persistTask(@Nullable @WebParam(name = "session", partName = "session") Session session,
                     @Nullable @WebParam(name = "task", partName = "task") Task task);

    @WebMethod
    @Nullable Task mergeTask(@Nullable @WebParam(name = "session", partName = "session") Session session,
                             @Nullable @WebParam(name = "task", partName = "task") Task task);

    @WebMethod
    @Nullable Task insertTask(@Nullable @WebParam(name = "session", partName = "session") Session session,
                              @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
                              @Nullable @WebParam(name = "taskName", partName = "taskName") String taskName,
                              @Nullable @WebParam(name = "descriptionTask", partName = "descriptionTask") String descriptionTask,
                              @Nullable @WebParam(name = "dateBeginTask", partName = "dateBeginTask") String dateBeginTask,
                              @Nullable @WebParam(name = "dateEndTask", partName = "dateEndTask") String dateEndTask);

    @WebMethod
    @Nullable Task removeTask(@Nullable @WebParam(name = "session", partName = "session") Session session,
                              @Nullable @WebParam(name = "taskId", partName = "taskId") String taskId);

    @WebMethod
    @Nullable Task findOneTask(@Nullable @WebParam(name = "session", partName = "session") Session session,
                               @Nullable @WebParam(name = "taskId", partName = "taskId") String taskId);

    @WebMethod
    @Nullable Collection<Task> findAllTaskByUserId(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Task updateTask(@Nullable @WebParam(name = "session", partName = "session") Session session,
                              @Nullable @WebParam(name = "taskId", partName = "taskId") String taskId,
                              @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
                              @Nullable @WebParam(name = "taskName", partName = "taskName") String taskName,
                              @Nullable @WebParam(name = "description", partName = "description") String description,
                              @Nullable @WebParam(name = "dateBegin", partName = "dateBegin") String dateBegin,
                              @Nullable @WebParam(name = "dateEnd", partName = "dateEnd") String dateEnd);

    @WebMethod
    void removeAllTask(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Task> findAllTaskInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Task> findAllTaskInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Task> findAllTaskInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") Session session);

    @WebMethod
    @Nullable Collection<Task> findTaskByPartNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") Session session,
                                                               @Nullable @WebParam(name = "part", partName = "part") String part);

    @WebMethod
    @Nullable Collection<Task> findAllTask(@Nullable @WebParam(name = "session", partName = "session") Session session);
}
