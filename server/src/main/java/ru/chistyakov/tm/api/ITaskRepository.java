package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task insert(@NotNull String userId, @NotNull String projectId, @NotNull String taskName,
                   @Nullable String descriptionTask, @Nullable Date dateBeginTask, @Nullable Date dateEndTask);

    Task persist(@NotNull Task task);

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String taskId);

    Task update(@NotNull String userId, @NotNull String taskId, @NotNull String projectId, @NotNull String newName,
                   @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    @NotNull
    Collection<Task> findAllInOrder(@NotNull String idUser, Comparator<Task> comparator);

    Task remove(@NotNull String userId, @NotNull String taskId);

    boolean removeByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAll(@NotNull String userId);

    @Nullable
    Collection<Task> findAll(@NotNull String userId);

    Task merge(@NotNull Task task);

    @Nullable
    Collection<Task> findByPartNameOrDescription(String idUser, String part);

    Collection<Task> findAll();

    List<Task> getList();
}
