package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;


public interface ITaskService extends IService<Task> {


    Task insert(@Nullable String userId, @Nullable String projectId, @Nullable String taskName, @Nullable String descriptionTask,
                @Nullable String dateBeginTask, @Nullable String dateEndTask);


    Task persist(@Nullable Task task);


    Task merge(@Nullable Task task);

    @Nullable
    Task findOne(@NotNull String userId, @Nullable String taskId);


    @Nullable
    Collection<Task> findAll(@Nullable String userId);


    Task update(@Nullable String userId, @Nullable String taskId, @Nullable String projectId,
                   @Nullable String newName, @Nullable String description, @Nullable String dateBegin,
                   @Nullable String dateEnd);


    Task remove(@NotNull String userId, @NotNull String taskId);


    @Nullable
    Collection<Task> findAllInOrderDateBegin(@NotNull String idUser);


    @Nullable
    Collection<Task> findAllInOrderDateEnd(@NotNull String idUser);


    @Nullable
    Collection<Task> findAllInOrderReadinessStatus(@NotNull String idUser);


    @Nullable
    Collection<Task> findByPartNameOrDescription(@NotNull String idUser, @NotNull String part);

    void removeAll(@NotNull String userId);
}
