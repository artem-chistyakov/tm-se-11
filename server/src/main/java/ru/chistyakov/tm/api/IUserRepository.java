package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User insert(@NotNull String login, @NotNull String password);

    User insertAdmin(@NotNull String login, @NotNull String password);

    @Nullable
    User findOne(@NotNull String id);

    User remove(@NotNull String id);

    User persist(@NotNull User user);

    User merge(@NotNull User user);

    User update(@NotNull String id, @NotNull String login, @NotNull String password, @Nullable RoleType roleType);

    User update(@NotNull String id, @NotNull String login, @NotNull String password);

    @Nullable
    User findUserByLoginAndPassword(@NotNull String login, @NotNull String passwordMD5);

    @NotNull
    Collection<User> findAll();

    @Nullable
    List<User> getList();
}
