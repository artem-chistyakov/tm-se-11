package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.User;

public interface IUserService extends IService<User> {

    @Nullable
    User authoriseUser(@NotNull String name, @NotNull String password);

    User updateUser(@NotNull String userId, @NotNull String login, @NotNull String password);

    User updatePassword(@NotNull String userId, @NotNull String password);

    User registryAdmin(@NotNull String login, @NotNull String password);

    User registryUser(@NotNull String login, @NotNull String password);

    User findById(final String userId);
}
