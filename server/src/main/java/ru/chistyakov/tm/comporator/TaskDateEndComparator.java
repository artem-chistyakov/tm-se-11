package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Task;

import java.util.Comparator;

public final class TaskDateEndComparator implements Comparator<Task> {
    @Override
    public int compare(@Nullable final Task o1, @Nullable final Task o2) {
        if (o1 == null || o2 == null) throw new NullPointerException();
        if (o1.getDateEndTask() == null) return -1;
        if (o2.getDateEndTask() == null) return 1;
        int resultComparing = o1.getDateEndTask().compareTo(o2.getDateEndTask());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        else return resultComparing;
    }
}
