package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.IProjectEndpoint")
@XmlRootElement
public final class ProjectEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.IProjectEndpoint {

    @NotNull
    private final ISessionService sessionService;
    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(@NotNull final IProjectService projectService, @NotNull final ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
        this.URL = "http://localhost:8080/ProjectEndpoint?wsdl";
    }

    @Override
    @WebMethod
    @Nullable
    public Project persistProject(@Nullable final @WebParam(name = "session", partName = "session") Session session,
                                  @Nullable final @WebParam(name = "project", partName = "project") Project project) {
        if (!sessionService.isValid(session)) return null;
        else return projectService.persist(project);
    }

    @Override
    @WebMethod
    @Nullable
    public Project mergeProject(@Nullable final @WebParam(name = "session", partName = "session") Session session,
                                @Nullable final @WebParam(name = "project", partName = "project") Project project) {
        if (!sessionService.isValid(session)) return null;
        else return projectService.merge(project);
    }

    @Override
    @WebMethod
    @Nullable
    public Project insertProject(@Nullable final @WebParam(name = "session", partName = "session") Session session,
                                 @Nullable final @WebParam(name = "name", partName = "name") String name,
                                 @Nullable final @WebParam(name = "descriptionProject", partName = "descriptionProject") String descriptionProject,
                                 @Nullable final @WebParam(name = "dateBeginProject", partName = "dateBeginProject") String dateBeginProject,
                                 @Nullable final @WebParam(name = "dateEndProject", partName = "dateEndProject") String dateEndProject) {
        if(session == null) return null;
        if (!sessionService.isValid(session)) return null;
        else return projectService.insert(session.getUserId(), name, descriptionProject, dateBeginProject, dateEndProject);
    }

    @Override
    @WebMethod
    @Nullable
    public Project findOneProject(@Nullable final @WebParam(name = "session", partName = "session") Session session,
                                  @Nullable final @WebParam(name = "projectId", partName = "projectId") String projectId) {
        if(session == null) return null;
        if (!sessionService.isValid(session)) return null;
        else return projectService.findOne(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @Nullable
    public Project removeProject(@Nullable final @WebParam(name = "session", partName = "session") Session session,
                                 @Nullable final @WebParam(name = "projectId", partName = "projectId") String projectId) {
        if(session == null) return null;
        if (!sessionService.isValid(session)) return null;
        else return projectService.remove(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @Nullable
    public Project updateProject(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                                 @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId,
                                 @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName,
                                 @Nullable @WebParam(name = "descriptionProject", partName = "descriptionProject") final String descriptionProject,
                                 @Nullable @WebParam(name = "dateBegin", partName = "dateBegin") final String dateBegin,
                                 @Nullable @WebParam(name = "dateEnd", partName = "dateEnd") final String dateEnd) {
        if (!sessionService.isValid(session)) return null;
        else return projectService.update(projectId, projectName, descriptionProject, dateBegin, dateEnd);
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Project> findAllProjectByUserId(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if(session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Project> findAllProject(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return null;
        return projectService.findAll();
    }


    @Override
    @WebMethod
    @Nullable
    public Collection<Project> findAllProjectInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if(session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findAllInOrderDateBegin(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Project> findAllProjectInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if(session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findAllInOrderDateEnd(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Project> findAllProjectInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if(session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findAllInOrderReadinessStatus(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Project> findByPartProjectNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                                                                  @Nullable @WebParam(name = "part", partName = "part") final String part) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findByPartNameOrDescription(session.getUserId(), part);
    }

    @Override
    @WebMethod
    public void removeAllProject(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if(session == null) return;
        if (!sessionService.isValid(session)) return;
        projectService.removeAll(session.getUserId());
    }
}