package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;

@WebService(endpointInterface = "ru.chistyakov.tm.api.ISessionEndpoint")
@XmlRootElement
public final class SessionEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.ISessionEndpoint {

    @NotNull
    private final ISessionService sessionService;

    public SessionEndpoint(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
        this.URL = "http://localhost:8080/SessionEndpoint?wsdl";
    }

    @Override
    @Nullable
    @WebMethod
    public Session openSession(@Nullable @WebParam(name = "userId", partName = "userId") final String userId,
                               @Nullable @WebParam(name = "roleType", partName = "roleType") final RoleType roleType) {
        if (userId == null || userId.isEmpty()) return null;
        return sessionService.open(userId, roleType);
    }

    @Override
    @Nullable
    @WebMethod
    public Session closeSession(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (session == null) return null;
        return sessionService.close(session);
    }
}
