package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.ITaskEndpoint")
@XmlRootElement
public final class TaskEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.ITaskEndpoint {
    @NotNull
    private final ISessionService sessionService;
    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull final ITaskService taskService, @NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
        this.taskService = taskService;
        this.URL = "http://localhost:8080/TaskEndpoint?wsdl";
    }

    @Override
    @WebMethod
    public Task persistTask(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                            @Nullable @WebParam(name = "task", partName = "task") final Task task) {
        if (!sessionService.isValid(session)) return null;
        else return taskService.persist(task);
    }

    @Override
    @WebMethod
    @Nullable
    public Task mergeTask(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                          @Nullable @WebParam(name = "task", partName = "task") final Task task) {
        if (!sessionService.isValid(session)) return null;
        else return taskService.merge(task);
    }

    @Override
    @WebMethod
    @Nullable
    public Task insertTask(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                           @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId,
                           @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName,
                           @Nullable @WebParam(name = "descriptionTask", partName = "descriptionTask") final String descriptionTask,
                           @Nullable @WebParam(name = "dateBeginTask", partName = "dateBeginTask") final String dateBeginTask,
                           @Nullable @WebParam(name = "dateEndTask", partName = "dateEndTask") final String dateEndTask) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return taskService.insert(session.getUserId(), projectId, taskName, descriptionTask, dateBeginTask, dateEndTask);
    }

    @Override
    @WebMethod
    @Nullable
    public Task removeTask(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                           @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (taskId == null || taskId.isEmpty() || session.getUserId() == null) return null;
        return taskService.remove(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    @Nullable
    public Task findOneTask(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                            @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null || taskId == null || taskId.isEmpty()) return null;
        return taskService.findOne(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Task> findAllTaskByUserId(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return taskService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Task updateTask(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                           @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId,
                           @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId,
                           @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName,
                           @Nullable @WebParam(name = "description", partName = "description") final String description,
                           @Nullable @WebParam(name = "dateBegin", partName = "dateBegin") final String dateBegin,
                           @Nullable @WebParam(name = "dateEnd", partName = "dateEnd") final String dateEnd) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return taskService.update(session.getUserId(), taskId, projectId, taskName, description, dateBegin, dateEnd);
    }

    @Override
    @WebMethod
    public void removeAllTask(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (session == null) return;
        if (!sessionService.isValid(session)) return;
        if (session.getUserId() == null) return;
        taskService.removeAll(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Task> findAllTaskInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null) return null;
        return taskService.findAllInOrderDateBegin(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Task> findAllTaskInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null) return null;
        return taskService.findAllInOrderDateEnd(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Task> findAllTaskInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (session == null || session.getUserId() == null) return null;
        if (!sessionService.isValid(session)) return null;
        return taskService.findAllInOrderReadinessStatus(session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable Collection<Task> findTaskByPartNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                                                                      @Nullable @WebParam(name = "part", partName = "part") final String part) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null || part == null || part.isEmpty()) return null;
        return taskService.findByPartNameOrDescription(session.getUserId(), part);
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<Task> findAllTask(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return null;
        return taskService.findAll();
    }
}
