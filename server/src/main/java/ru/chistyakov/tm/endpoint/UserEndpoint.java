package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IDomainService;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.IUserEndpoint")
@XmlRootElement
public final class UserEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.IUserEndpoint {

    @NotNull
    private final IDomainService domainService;
    @NotNull
    private final ISessionService sessionService;
    @NotNull
    private final IUserService userService;

    public UserEndpoint(@NotNull final IUserService userService, @NotNull final ISessionService sessionService, @NotNull final IDomainService domainService) {
        this.sessionService = sessionService;
        this.userService = userService;
        this.domainService = domainService;
        this.URL = "http://localhost:8080/UserEndpoint?wsdl";
    }

    @Override
    @WebMethod
    public User registryUser(@Nullable @WebParam(name = "login", partName = "login") final String login,
                             @Nullable @WebParam(name = "password", partName = "password") final String password) {
        if (login == null || password == null) return null;
        return userService.registryUser(login, password);
    }

    @Override
    @WebMethod
    @Nullable
    public Session authorizeUser(@Nullable @WebParam(name = "login", partName = "login") final String login,
                                 @Nullable @WebParam(name = "password", partName = "password") final String password) {
        if (login == null || password == null) return null;
        final User user = userService.authoriseUser(login, password);
        if (user == null) return null;
        else {
            final Session session = sessionService.open(user.getId(), user.getRoleType());
            if (session == null) return null;
            return session;
        }
    }

    @Override
    @WebMethod
    @Nullable
    public User updateUser(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                           @Nullable @WebParam(name = "login", partName = "login") final String login,
                           @Nullable @WebParam(name = "password", partName = "password") final String password) {
        if(session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null || login == null || password == null || login.isEmpty() || password.isEmpty()) return null;
        else return userService.updateUser(session.getUserId(), login, password);
    }

    @Override
    @WebMethod
    @Nullable
    public User persistUser(@Nullable @WebParam(name = "user", partName = "user") final User user) {
        return userService.persist(user);
    }

    @Override
    @WebMethod
    @Nullable
    public User updateUserPassword(@Nullable @WebParam(name = "session", partName = "session") final Session session,
                                   @Nullable @WebParam(name = "password", partName = "password") final String password) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null || password == null || password.isEmpty()) return null;
        else return userService.updatePassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    @Nullable
    public User findUserById(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return userService.findById(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<User> findAllUser(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return null;
        return userService.findAll();
    }

    @Override
    @WebMethod
    @Nullable
    public User registryAdmin(@Nullable @WebParam(name = "login", partName = "login") final String login,
                              @Nullable @WebParam(name = "password", partName = "password") final String password) {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) return null;
        return userService.registryAdmin(login, password);
    }

    @Override
    @WebMethod
    public void serializateDomain(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return;
        domainService.serializateDomain();
    }

    @Override
    @WebMethod
    public void deserializateDomain(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return;
        domainService.deserializateDomain();
    }

    @Override
    @WebMethod
    public void saveDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return;
        domainService.saveDomainJacksonXml();
    }

    @Override
    @WebMethod
    public void loadDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return;
        domainService.loadDomainJacksonXml();
    }

    @Override
    @WebMethod
    public void saveDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return;
        domainService.saveDomainJacksonJson();
    }

    @Override
    @WebMethod
    public void loadDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") final Session session) {
        if (!sessionService.isValid(session)) return;
        domainService.loadDomainJacksonJson();
    }
}
