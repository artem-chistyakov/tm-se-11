package ru.chistyakov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity implements Cloneable {

    @Nullable
    private String userId;
    @NotNull
    private Long timestamp = System.currentTimeMillis();
    @Nullable
    private String signature;
    @Nullable
    private RoleType roleType;

    @Override
    public Object clone() {
        try {
            Object clone = super.clone();
            Session session = (Session) clone;
            session.setId(this.id);
            session.setUserId(this.userId);
            session.setRoleType(this.roleType);
            session.setTimestamp(this.timestamp);
            session.setSignature(null);
            return session;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

