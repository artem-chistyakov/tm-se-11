package ru.chistyakov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "task")
public final class Task extends AbstractEntity implements Serializable {

    @Nullable
    private String userId;
    @Nullable
    private String projectId;
    @NotNull
    private String name = "";
    @NotNull
    private ReadinessStatus readinessStatus = ReadinessStatus.PLANNED;
    @Nullable
    private String description;
    @Nullable
    private Date dateBeginTask;
    @Nullable
    private Date dateEndTask;
}
