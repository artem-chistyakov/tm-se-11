package ru.chistyakov.tm.enumerate;

public enum RoleType {

    ADMINISTRATOR("admin"), USUAL_USER("user");

    private final String login;

    RoleType(String login) {
        this.login = login;
    }

    public String displaylogin() {
        return login;
    }
}
