package ru.chistyakov.tm.loader;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.endpoint.ProjectEndpoint;
import ru.chistyakov.tm.endpoint.SessionEndpoint;
import ru.chistyakov.tm.endpoint.TaskEndpoint;
import ru.chistyakov.tm.endpoint.UserEndpoint;
import ru.chistyakov.tm.repository.ProjectRepository;
import ru.chistyakov.tm.repository.SessionRepository;
import ru.chistyakov.tm.repository.TaskRepository;
import ru.chistyakov.tm.repository.UserRepository;
import ru.chistyakov.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private PropertiesService propertiesService = new PropertiesService();
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);
    @NotNull
    private final IDomainService domainService = new DomainService(projectRepository, userRepository, taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository(propertiesService);
    @NotNull
    private final IUserService userService = new UserService(userRepository, sessionRepository);
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this, propertiesService);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(userService, sessionService, domainService);

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    private void init() {
        propertiesService.init();
        Endpoint.publish(projectEndpoint.getURL(), projectEndpoint);
        Endpoint.publish(taskEndpoint.getURL(), taskEndpoint);
        Endpoint.publish(userEndpoint.getURL(), userEndpoint);
        Endpoint.publish(sessionEndpoint.getURL(), sessionEndpoint);
    }

    public void start() {
        init();
    }
}

