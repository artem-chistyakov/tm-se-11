package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;


public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    @NotNull
    final Map<String, T> entities = new LinkedHashMap<>();

    @NotNull
    public Map<String, T> getEntities() {
        return entities;
    }

    @Nullable
    public abstract T persist(T t);

    @Nullable
    public abstract T merge(T t);

    @NotNull
    public Collection<T> findAll() {
        return new ArrayList<>(entities.values());
    }

}
