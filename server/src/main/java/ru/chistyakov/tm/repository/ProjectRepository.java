package ru.chistyakov.tm.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.entity.Project;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {


    @Override
    @Nullable
    public Project insert(@NotNull final String userId, @NotNull final String name,
                          @Nullable final String description,
                          @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd) {
        final Project project = new Project();
        if (entities.containsKey(project.getId())) return null;
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateBeginProject(dateBegin);
        project.setDateEndProject(dateEnd);
        entities.put(project.getId(), project);
        return project;
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) {
        for (final Project project : entities.values())
            if (userId.equals(project.getUserId()) && projectId.equals(project.getId())) return project;
        return null;
    }

    @Override
    @NotNull
    public Collection<Project> findAllInOrder(@NotNull final String idUser, @NotNull final Comparator<Project> comparator) {
        final TreeSet<Project> treeSet = new TreeSet<>(comparator);
        treeSet.addAll(findAll(idUser));
        return treeSet;
    }

    @Override
    @NotNull
    public Collection<Project> findAll(@NotNull final String userId) {
        final Collection<Project> projectCollection = new ArrayList<>();
        for (final Project project : entities.values())
            if (userId.equals(project.getUserId())) projectCollection.add(project);
        return projectCollection;
    }

    @Override
    @Nullable
    public Project update(@NotNull final String projectId, @NotNull final String projectName,
                          @Nullable final String description, @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd) {
        if (!entities.containsKey(projectId)) return null;
        final Project project = entities.get(projectId);
        project.setName(projectName);
        project.setDescription(description);
        project.setDateBeginProject(dateBegin);
        project.setDateEndProject(dateEnd);
        return project;
    }

    @Override
    @Nullable
    public Project remove(@NotNull final String userId, @NotNull final String projectId) {
        for (final Project project : entities.values())
            if (userId.equals(project.getUserId()) || projectId.equals(project.getId())) {
                return entities.remove(projectId);
            }
        return null;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        for (final Project project : entities.values())
            if (userId.equals(project.getUserId())) entities.remove(project.getId());
    }

    @Override
    public Project persist(@NotNull final Project project) {
        if (project.getUserId() == null) return null;
        return insert(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getDateBeginProject(), project.getDateEndProject());
    }

    @Nullable
    private Project insert(@Nullable final String userId, @Nullable final String id, @Nullable final String name,
                           @Nullable final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        if (userId == null || id == null || name == null) return null;
        final Project project = new Project();
        project.setUserId(userId);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setDateBeginProject(dateBegin);
        project.setDateEndProject(dateEnd);
        entities.put(id, project);
        return project;
    }

    @Override
    @Nullable
    public Project merge(@NotNull final Project project) {
        if (project.getUserId() == null) return null;
        if (entities.get(project.getId()) == null)
            return insert(project.getUserId(), project.getName(), project.getDescription(), project.getDateBeginProject(), project.getDateEndProject());
        else
            return update(project.getId(), project.getName(), project.getDescription(), project.getDateBeginProject(), project.getDateEndProject());

    }

    @Override
    @NotNull
    public Collection<Project> findByPartNameOrDescription(@NotNull final String idUser, @NotNull final String part) {
        final Collection<Project> collection = new ArrayList<>();
        for (final Project project : entities.values()) {
            if (project.getDescription() == null) {
                if (project.getName().contains(part)) collection.add(project);
            } else if (project.getName().contains(part) || project.getDescription().contains(part))
                collection.add(project);
        }
        return collection;
    }

    @Override
    public @NotNull List<Project> getList() {
        return new ArrayList<>(entities.values());
    }
}
