package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionRepository;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.service.PropertiesService;
import ru.chistyakov.tm.utility.SignatureUtil;

import java.util.LinkedHashMap;
import java.util.Map;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    final private Map<String, Session> sessionMap = new LinkedHashMap<>();

    @NotNull
    public Map<String, Session> getSessionMap() {
        return sessionMap;
    }

    @NotNull
    private final PropertiesService propertiesService;

    public SessionRepository(@NotNull final PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    @Override
    @NotNull
    public Session create(@Nullable final String userId, @Nullable final RoleType roleType) {
        final Session session = new Session();
        session.setUserId(userId);
        session.setRoleType(roleType);
        session.setSignature(SignatureUtil.sign(session, propertiesService.getSALT(), propertiesService.getCycle()));
//        session.setSignature(SignatureUtil.sign(session,"qwerty", 2020));
        sessionMap.put(session.getId(), session);
        return session;
    }

    @Override
    public boolean contains(@Nullable final String sessionId) {
        return sessionMap.get(sessionId) != null;
    }

    @Override
    @Nullable
    public Session delete(@Nullable final String sessionId) {
        return sessionMap.remove(sessionId);
    }
}
