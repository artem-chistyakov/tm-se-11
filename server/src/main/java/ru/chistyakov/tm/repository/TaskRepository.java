package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task insert(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName,
                       @Nullable final String descriptionTask, @Nullable final Date dateBeginTask,
                       @Nullable final Date dateEndTask) {
        if (taskName == null || taskName.isEmpty()) return null;
        Task newTask = new Task();
        if (entities.containsKey(newTask.getId())) return null;
        newTask.setUserId(userId);
        newTask.setName(taskName);
        newTask.setProjectId(projectId);
        newTask.setDescription(descriptionTask);
        newTask.setDateBeginTask(dateBeginTask);
        newTask.setDateEndTask(dateEndTask);
        entities.put(newTask.getId(), newTask);
        return newTask;
    }

    @Override
    public Task persist(@Nullable final Task task) {
        if (task == null) return null;
        if (task.getUserId() == null || task.getProjectId() == null) return null;
        return insert(task.getUserId(), task.getProjectId(), task.getId(), task.getName(), task.getDescription(),
                task.getDateBeginTask(), task.getDateEndTask());
    }

    @Nullable
    private Task insert(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId,
                        @Nullable final String name, @Nullable final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        if (userId == null || projectId == null || name == null) return null;
        final Task task = new Task();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setId(taskId);
        task.setName(name);
        task.setDescription(description);
        task.setDateBeginTask(dateBegin);
        task.setDateEndTask(dateEnd);
        entities.put(taskId, task);
        return task;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String userId, @Nullable final String idTask) {
        if (userId == null || idTask == null || userId.isEmpty() || idTask.isEmpty()) return null;
        return findById(userId, idTask);
    }

    @Override
    public Task update(@Nullable final String userId, @Nullable final String idTask, @Nullable final String projectId,
                       @Nullable final String name, @Nullable final String description, @Nullable final Date dateBegin,
                       @Nullable final Date dateEnd) {
        if (userId == null || idTask == null || userId.isEmpty() || idTask.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findById(userId, idTask);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        task.setProjectId(projectId);
        task.setName(name);
        task.setDateEndTask(dateEnd);
        task.setDateBeginTask(dateBegin);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || taskId == null) return null;
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId()) && taskId.equals(task.getId())) {
                return entities.remove(taskId);
            }
        return null;
    }

    @Override
    public boolean removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        final List<String> listTaskId = findTaskIdByProjectId(userId, projectId);
        if (listTaskId.isEmpty()) return false;
        for (final String str : listTaskId) entities.remove(str);
        return true;
    }

    @Nullable
    private Task findById(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || taskId == null) return null;
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId()) && taskId.equals(task.getId())) return task;
        return null;
    }

    @Nullable
    private List<String> findTaskIdByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        final List<String> tasksIdForFind = new ArrayList<>();
        for (final Task task : entities.values())
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId()))
                tasksIdForFind.add(task.getId());
        return tasksIdForFind;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        for (final Task task : entities.values()) if (userId.equals(task.getUserId())) entities.remove(task.getId());
    }

    @Nullable
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        final Collection<Task> collection = new ArrayList<>();
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId())) collection.add(task);
        return collection;
    }

    @Override
    @Nullable
    public Task merge(@Nullable final Task task) {
        if (task == null) return null;
        if (task.getUserId() == null || task.getProjectId() == null) return null;
        if (entities.get(task.getId()) == null)
            return insert(task.getUserId(), task.getProjectId(), task.getName(), task.getDescription(),
                    task.getDateBeginTask(), task.getDateEndTask());
        else
            return update(task.getUserId(), task.getId(), task.getProjectId(), task.getName(), task.getDescription(),
                    task.getDateBeginTask(), task.getDateEndTask());
    }

    @Override
    public @NotNull Collection<Task> findAllInOrder(@Nullable final String idUser, @Nullable final Comparator<Task> comparator) {
        TreeSet<Task> treeSet = new TreeSet<>(comparator);
        treeSet.addAll(findAll(idUser));
        return treeSet;
    }


    @Override
    public @Nullable Collection<Task> findByPartNameOrDescription(@Nullable final String idUser, @Nullable final String part) {
        if (part == null) return null;
        final Collection<Task> collection = new ArrayList<>();
        for (Task task : entities.values()) {
            if (task.getDescription() == null) {
                if (task.getName().contains(part)) collection.add(task);
            } else if (task.getName().contains(part) || task.getDescription().contains(part))
                collection.add(task);
        }
        return collection;
    }

    public List<Task> getList() {
        return new ArrayList<>(entities.values());
    }
}

