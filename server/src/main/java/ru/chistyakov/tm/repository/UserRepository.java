package ru.chistyakov.tm.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserRepository;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User insert(@NotNull final String login, @NotNull final String password) {
        if (findByLogin(login) != null) return null;
        final User user = new User();
        if (entities.containsKey(user.getId())) return null;
        user.setRoleType(RoleType.USUAL_USER);
        user.setLogin(login);
        user.setPassword(password);
        entities.put(user.getId(), user);
        return user;
    }

    @Nullable
    private User insert(@Nullable final String userId, @Nullable final String login, @Nullable final String password, @Nullable RoleType roleType) {
        if (userId == null || login == null || password == null) return null;
        final User user = new User();
        user.setId(userId);
        user.setLogin(login);
        user.setPassword(password);
        if (roleType == null) user.setRoleType(RoleType.USUAL_USER);
        else user.setRoleType(roleType);
        entities.put(userId, user);
        return user;
    }

    @Override
    @Nullable
    public User findUserByLoginAndPassword(@NotNull final String login, @NotNull final String passwordMD5) {
        final User user = findByLogin(login);
        if (user == null) return null;
        if (passwordMD5.equals(user.getPassword())) return user;
        return null;
    }

    @Override
    @Nullable
    public User insertAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return null;
        if (findByLogin(login) != null) return null;
        final User user = new User();
        if (entities.containsKey(user.getId())) return null;
        user.setRoleType(RoleType.ADMINISTRATOR);
        user.setLogin(login);
        user.setPassword(password);
        return entities.put(user.getId(), user);
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) {
        return entities.get(id);
    }

    @Override
    public User remove(@Nullable final String id) {
        return entities.remove(id);
    }

    @Override
    public User persist(@Nullable final User user) {
        if (user == null) return null;
        if (entities.containsKey(user.getId()))
            return update(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
        return insert(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
    }

    @Override
    @Nullable
    public User merge(@Nullable final User user) {
        if (user == null) return null;
        if (entities.get(user.getId()) == null) return
                insert(user.getLogin(), user.getPassword());
        else
            return update(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
    }

    @Nullable
    private User findByLogin(@Nullable String login) {
        if (login == null) return null;
        for (User user : entities.values()) if (login.equals(user.getLogin())) return user;
        return null;
    }

    @Override
    public User update(@Nullable final String id, @Nullable final String login, @Nullable final String password, @Nullable RoleType roleType) {
        if (login == null || password == null) return null;
        final User user = entities.get(id);
        if (user == null) return null;
        user.setLogin(login);
        user.setPassword(password);
        if (roleType == null) user.setRoleType(RoleType.USUAL_USER);
        else user.setRoleType(roleType);
        return user;
    }

    @Override
    public User update(@NotNull final String id, @NotNull final String login, @NotNull final String password) {
        final User user = entities.get(id);
        if (user == null) return null;
        user.setLogin(login);
        user.setPassword(password);
        return user;
    }

    @Override
    public List<User> getList() {
        return new ArrayList<>(entities.values());
    }
}
