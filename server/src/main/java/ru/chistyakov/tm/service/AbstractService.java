package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.IService;
import ru.chistyakov.tm.entity.AbstractEntity;
import ru.chistyakov.tm.entity.Project;

import java.text.SimpleDateFormat;
import java.util.Collection;


public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public abstract T merge(T t);

    public abstract T persist(T t);

    @NotNull
    public abstract Collection<T> findAll();
}
