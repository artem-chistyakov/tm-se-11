package ru.chistyakov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.api.IUserRepository;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;

import java.io.*;
import java.util.Collection;
import java.util.Iterator;

public class DomainService implements ru.chistyakov.tm.api.IDomainService {
    @NotNull
    private final IProjectRepository projectRepository;
    @NotNull
    private final IUserRepository userRepository;
    @NotNull
    private final ITaskRepository taskRepository;

    public DomainService(@NotNull final IProjectRepository projectRepository, @NotNull final IUserRepository userRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void serializateDomain() {
        try (@NotNull final FileOutputStream projectFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "projectFile.txt");
             @NotNull final FileOutputStream taskFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "taskFile.txt");
             @NotNull final FileOutputStream userFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userFile.txt")) {
            final ObjectOutputStream projectObjectOutputStream = new ObjectOutputStream(projectFileOutputStream);
            projectObjectOutputStream.writeObject(projectRepository.findAll());
            final ObjectOutputStream taskObjectOutputStream = new ObjectOutputStream(taskFileOutputStream);
            taskObjectOutputStream.writeObject(taskRepository.findAll());
            final ObjectOutputStream userObjectOutputStream = new ObjectOutputStream(userFileOutputStream);
            userObjectOutputStream.writeObject(userRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserializateDomain() {
        try (final FileInputStream userFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "projectFile.txt");
             final FileInputStream projectFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "taskFile.txt");
             final FileInputStream taskFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "userFile.txt")) {
            final ObjectInputStream userObjectInputStream = new ObjectInputStream(userFileInputStream);
            final ObjectInputStream projectObjectInputStream = new ObjectInputStream(projectFileInputStream);
            final ObjectInputStream taskObjectInputStream = new ObjectInputStream(taskFileInputStream);
            final Collection<User> userCollection = (Collection<User>) userObjectInputStream.readObject();
            for (final User user : userCollection) {
                userRepository.persist(user);
            }
            final Collection<Project> projectCollection = (Collection<Project>) projectObjectInputStream.readObject();
            for (final Project project : projectCollection) {
                projectRepository.persist(project);
            }
            final Collection<Task> taskCollection = (Collection<Task>) taskObjectInputStream.readObject();
            for (Task task : taskCollection) {
                taskRepository.persist(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDomainJacksonXml() {
        try (FileOutputStream fOSProject = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
             FileOutputStream fOSTask = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
             FileOutputStream fOSUser = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userXml.xml")) {
            final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            final XmlMapper xmlMapper = new XmlMapper(module);
            final String projectJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectRepository.findAll());
            final String taskJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskRepository.findAll());
            final String userJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userRepository.findAll());
            fOSProject.write(projectJsonArray.getBytes());
            fOSProject.flush();
            fOSTask.write(taskJsonArray.getBytes());
            fOSTask.flush();
            fOSUser.write(userJsonArray.getBytes());
            fOSUser.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadDomainJacksonXml() {
        try {
            final JacksonXmlModule jacksonXmlModule = new JacksonXmlModule();
            jacksonXmlModule.setDefaultUseWrapper(false);
            final XmlMapper xmlMapper = new XmlMapper(jacksonXmlModule);
            final Iterator<Project> projectIterator = xmlMapper.readerFor(Project.class).readValues(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
            final Iterator<Task> taskIterator = xmlMapper.readerFor(Task.class).readValues(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
            final Iterator<User> userIterator = xmlMapper.readerFor(User.class).readValues(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
            while (userIterator.hasNext()) {
                userRepository.persist(userIterator.next());
            }
            while (projectIterator.hasNext()) {
                projectRepository.persist(projectIterator.next());
            }
            while (taskIterator.hasNext()) {
                taskRepository.persist(taskIterator.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDomainJacksonJson() {
        try (FileOutputStream fOSProject = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userJson.json");
             FileOutputStream fOSTask = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userJson.json");
             FileOutputStream fOSUser = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userJson.json")) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String projectJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectRepository.findAll());
            final String taskJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskRepository.findAll());
            final String userJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userRepository.findAll());
            fOSProject.write(projectJsonArray.getBytes());
            fOSTask.write(taskJsonArray.getBytes());
            fOSUser.write(userJsonArray.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadDomainJacksonJson() {
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final Iterator<Project> projectIterator = objectMapper.readerFor(Project.class).readValues(new File(".").getAbsolutePath() + File.separator + "userJson.json");
            final Iterator<Task> taskIterator = objectMapper.readerFor(Task.class).readValues(new File(".").getAbsolutePath() + File.separator + "userJson.json");
            final Iterator<User> userIterator = objectMapper.readerFor(User.class).readValues(new File(".").getAbsolutePath() + File.separator + "userJson.json");
            while (userIterator.hasNext()) {
                userRepository.persist(userIterator.next());
            }
            while (projectIterator.hasNext()) {
                projectRepository.persist(projectIterator.next());
            }
            while (taskIterator.hasNext()) {
                taskRepository.persist(taskIterator.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}