package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.comporator.ProjectDateBeginComparator;
import ru.chistyakov.tm.comporator.ProjectDateEndComparator;
import ru.chistyakov.tm.comporator.ProjectReadinessStatusComparator;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.utility.DateParser;

import java.util.Collection;

public final class ProjectService extends AbstractService<Project> implements IProjectService {
    @NotNull
    private final IProjectRepository projectRepository;
    @NotNull
    private final ITaskRepository taskRepository;


    public ProjectService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    @Nullable
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        return projectRepository.merge(project);
    }

    @Override
    @Nullable
    public Project persist(@Nullable final Project project) {
        if (project == null) return null;
        return projectRepository.persist(project);
    }


    @Override
    @Nullable
    public Project insert(@Nullable final String userId, @Nullable final String name,
                          @Nullable String descriptionProject, @Nullable String dateBeginProject,
                          @Nullable String dateEndProject) {
        if (userId == null || name == null) return null;
        if (name.trim().isEmpty() || userId.trim().isEmpty()) return null;
        return projectRepository.insert(
                userId, name, descriptionProject,
                DateParser.parseDate(simpleDateFormat, dateBeginProject),
                DateParser.parseDate(simpleDateFormat, dateEndProject));
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.trim().isEmpty()) return;
        projectRepository.removeAll(userId);
        taskRepository.removeAll(userId);
    }

    @Override
    @Nullable
    public Project remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        if (projectId.trim().isEmpty()) return null;
        taskRepository.removeByProjectId(userId, projectId);
        return projectRepository.remove(userId, projectId);
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        return projectRepository.findOne(projectId, userId);
    }


    @Override
    @Nullable
    public Project update(@Nullable final String projectId, @Nullable final String projectNameProject,
                          @Nullable final String descriptionProject, @Nullable final String dateBegin,
                          @Nullable final String dateEnd) {
        if (projectId == null || projectNameProject == null) return null;
        if (projectId.trim().isEmpty() || projectNameProject.trim().isEmpty()) return null;
        return projectRepository.update(projectId, projectNameProject, descriptionProject,
                DateParser.parseDate(simpleDateFormat, dateBegin),
                DateParser.parseDate(simpleDateFormat, dateEnd));
    }

    @Nullable
    @Override
    public Collection<Project> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findAllInOrder(userId, new ProjectDateBeginComparator());
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findAllInOrder(userId, new ProjectDateEndComparator());
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findAllInOrder(userId, new ProjectReadinessStatusComparator());
    }

    @Nullable
    @Override
    public Collection<Project> findByPartNameOrDescription(@Nullable final String idUser, @Nullable final String part) {
        if (idUser == null || part == null) return null;
        return projectRepository.findByPartNameOrDescription(idUser, part);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }
}
