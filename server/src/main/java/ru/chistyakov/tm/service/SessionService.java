package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionRepository;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.api.ServiceLocator;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.utility.SignatureUtil;

public final class SessionService implements ISessionService {
    @NotNull
    private final ISessionRepository sessionRepository;
    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private final PropertiesService propertiesService;

    public SessionService(@NotNull final ISessionRepository ISessionRepository, @NotNull final ServiceLocator serviceLocator,
                          @NotNull final PropertiesService propertiesService) {
        this.sessionRepository = ISessionRepository;
        this.serviceLocator = serviceLocator;
        this.propertiesService = propertiesService;
    }

    @NotNull
    public ISessionRepository getSessionRepository() {
        return sessionRepository;
    }

    @Override
    public void validate(@Nullable final Session session) throws IllegalAccessException {
        if (sessionRepository == null || serviceLocator == null) throw new IllegalAccessException();
        if (session == null) throw new IllegalAccessException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new IllegalAccessException();
        if (session.getTimestamp() == null) throw new IllegalAccessException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new IllegalAccessException();
        final String signatureSource = session.getSignature();
        Session session1 = (Session) session.clone();
        if (propertiesService.getSALT() == null || propertiesService.getCycle() == null)
            throw new IllegalArgumentException("Ошибка загрузки проперти файла");
        final String signatureTarget = SignatureUtil.sign(session1, propertiesService.getSALT(), propertiesService.getCycle());
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new IllegalAccessException();
        if (!sessionRepository.contains(session.getId())) throw new IllegalAccessException();
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final RoleType roleType) throws IllegalAccessException {
        if (session == null) throw new IllegalAccessException();
        if (roleType == null) throw new IllegalAccessException();
        session.setRoleType(roleType);
        validate(session);
        final String userId = session.getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new IllegalAccessException();
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean isValid(@Nullable final Session session, @Nullable final RoleType roleType) {
        try {
            validate(session, roleType);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Nullable
    public Session open(@Nullable final String userId, RoleType roleType) {
        if (userId == null || userId.isEmpty()) return null;
        return sessionRepository.create(userId, roleType);
    }

    @Override
    @Nullable
    public Session close(@Nullable final Session session) {
        if (sessionRepository == null || serviceLocator == null) return null;
        if (session == null) return null;
        return sessionRepository.delete(session.getId());
    }
}
