package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.comporator.TaskDateBeginComparator;
import ru.chistyakov.tm.comporator.TaskDateEndComparator;
import ru.chistyakov.tm.comporator.TaskReadinessStatusComparator;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.utility.DateParser;

import java.util.Collection;


public final class TaskService extends AbstractService<Task> implements ITaskService {
    @NotNull
    private final ITaskRepository taskRepository;
    @NotNull
    private final IProjectRepository projectRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository, @NotNull final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Task merge(@Nullable final Task task) {
        if (task == null) return null;
        return taskRepository.merge(task);
    }

    @Override
    public Task persist(@Nullable final Task task) {
        if (task == null) return null;
        return taskRepository.persist(task);
    }

    @Override
    public Task insert(@Nullable final String userId, @Nullable final String projectId,
                       @Nullable final String taskName, @Nullable final String descriptionTask,
                       @Nullable final String dateBeginTask, @Nullable final String dateEndTask) {
        if (userId == null || projectId == null || taskName == null) return null;
        if (userId.trim().isEmpty()) return null;
        if (taskName.trim().isEmpty() || projectId.trim().isEmpty() || projectRepository.findOne(userId, projectId) == null)
            return null;
        return taskRepository.insert(
                userId, projectId, taskName, descriptionTask,
                DateParser.parseDate(simpleDateFormat, dateBeginTask),
                DateParser.parseDate(simpleDateFormat, dateEndTask));

    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || taskId == null) return null;
        if (userId.trim().isEmpty() || taskId.trim().isEmpty()) return null;
        return taskRepository.findOne(userId, taskId);
    }

    @Nullable
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        return taskRepository.findAll(userId);
    }

    @Override
    public Task update(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId,
                       @Nullable final String taskName, @Nullable final String description, @Nullable final String dateBegin,
                       @Nullable final String dateEnd) {
        if (userId == null || taskId == null || projectId == null || taskName == null) return null;
        if (taskId.trim().isEmpty() || taskName.trim().isEmpty()) return null;
        if (projectRepository.findOne(userId, projectId) == null) return null;
        return taskRepository.update(
                userId, taskId, projectId, taskName, description,
                DateParser.parseDate(simpleDateFormat, dateBegin),
                DateParser.parseDate(simpleDateFormat, dateEnd));
    }

    @Override
    public Task remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || taskId == null) return null;
        if (userId.trim().isEmpty() || taskId.trim().isEmpty()) return null;
        return taskRepository.remove(userId, taskId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        taskRepository.removeAll(userId);
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        return taskRepository.findAllInOrder(userId, new TaskDateBeginComparator());
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        return taskRepository.findAllInOrder(userId, new TaskDateEndComparator());
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        return taskRepository.findAllInOrder(userId, new TaskReadinessStatusComparator());
    }

    @Override
    public @Nullable Collection<Task> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || part == null) return null;
        return taskRepository.findByPartNameOrDescription(userId, part);
    }

    @NotNull
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }
}
