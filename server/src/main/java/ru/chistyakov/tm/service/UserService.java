package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionRepository;
import ru.chistyakov.tm.api.IUserRepository;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.utility.PasswordParser;

import java.util.Collection;
import java.util.Objects;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final ISessionRepository sessionRepository;
    @NotNull
    private final IUserRepository userRepository;


    public UserService(@NotNull final IUserRepository userRepository, @NotNull final ISessionRepository sessionRepository) {
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
    }

    @Override
    @Nullable
    public User merge(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.merge(user);
    }

    @Override
    @Nullable
    public User persist(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.persist(user);
    }

    @Override
    @Nullable
    public User authoriseUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return null;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        return userRepository.findUserByLoginAndPassword(login, passwordMD5);

    }

    @Override
    @Nullable
    public User updateUser(@Nullable final String userId, @Nullable final String login, @Nullable final String password) {
        if (userId == null || login == null || password == null) return null;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        return userRepository.update(userId, login, passwordMD5);
    }

    @Override
    @Nullable
    public User updatePassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || password == null) return null;
        if (password.trim().isEmpty() || userId.isEmpty()) return null;
        User user = findById(userId);
        user.setPassword(Objects.requireNonNull(PasswordParser.parse(password)));
        return user;
    }

    @Override
    @Nullable
    public User registryAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return null;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        return userRepository.insertAdmin(login, passwordMD5);
    }

    @Override
    @Nullable
    public User registryUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return null;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        return userRepository.insert(login, passwordMD5);

    }

    @Override
    @Nullable
    public User findById(String userId) {
        return userRepository.findOne(userId);
    }

    @NotNull
    public Collection<User> findAll() {
        return userRepository.findAll();
    }
}
